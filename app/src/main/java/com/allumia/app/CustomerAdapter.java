package com.allumia.app;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.CustomerViewHolder> {
    private List<CustomerModel> mCustomerList;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(int i);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public static class CustomerViewHolder extends RecyclerView.ViewHolder {

        public TextView customerNameView;

        public CustomerViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            customerNameView = itemView.findViewById(R.id.customer_item_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public CustomerAdapter(List<CustomerModel> customerList) {
        mCustomerList = customerList;
    }

    @NonNull
    @Override
    public CustomerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.customer_item, viewGroup, false);
        CustomerViewHolder customerViewHolder = new CustomerViewHolder(v, listener);
        return customerViewHolder;
    }

    @Override
    public void onBindViewHolder(CustomerViewHolder viewHolder, int i) {
        CustomerModel currentItem = mCustomerList.get(i);

        viewHolder.customerNameView.setText(currentItem.getName());
    }

    @Override
    public int getItemCount() {
        return mCustomerList.size();
    }

}
