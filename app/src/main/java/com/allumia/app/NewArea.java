package com.allumia.app;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;


public class NewArea extends AppCompatActivity {
    public static final String TAG = "SaveNewAreaActivity";

    private EditText areaName;
    private EditText annualBurnHour;
    private CheckBox peakCoincident;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_area);
        Toolbar toolbar = findViewById(R.id.new_area_toolbar);
        setSupportActionBar(toolbar);

        areaName = findViewById(R.id.area_name_input);
        annualBurnHour = findViewById(R.id.annual_burn_hour_input);
        peakCoincident = findViewById(R.id.peak_coincident);

        Button saveButton = findViewById(R.id.new_area_save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                saveArea();
            }
        });

        Button addSpaceButton = findViewById(R.id.add_space_button);
        addSpaceButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                addSpace();
            }
        });
    }

    private void saveArea(){

    }

    private void addSpace(){

    }
}
