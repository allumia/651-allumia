package com.allumia.app;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.allumia.app.model.ProjectModel;
import com.allumia.app.model.SalespersonModel;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NewProject extends AppCompatActivity implements AdapterView.OnItemSelectedListener,NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "NewProject";
    private List<CustomerModel> customer;
    private List<SalespersonModel> salesperson;
    private EditText projectNameView;
    private Spinner spinnerCustomer;
    private Spinner spinnerSalesperson;
    private Button addAreaButton;
    private Button saveButton;

    private ProjectController controller;
    private int projectId;
    private boolean isSaved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_project);
        controller = new ProjectController(this);
        InitializeInfo();

        projectNameView = findViewById(R.id.project_name_input);
        addAreaButton = findViewById(R.id.add_area_button);
        addAreaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSaved) {
                    Intent AreaIntent = new Intent(NewProject.this, NewArea.class);
                    startActivity(AreaIntent);
                } else {
                    Toast toast = Toast.makeText(NewProject.this, "Please save project first", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
        saveButton = findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptUpload();
            }
        });

        NavigationView navigationView = findViewById(R.id.info_custom);
        navigationView.setNavigationItemSelectedListener(this);
        // Guide menu
        Toolbar toolbar = findViewById(R.id.custom_toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.Cus_drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        
        Intent intent = getIntent();
        projectId = intent.getIntExtra(getString(R.string.lighting_project_id), 0);
        isSaved = false;
        if (projectId != 0) {
            controller.getProjectDetail(projectId, new VolleyEventListener() {
                @Override
                public void onSuccess(JSONObject response) {
                    ProjectModel project = controller.buildProjectFromJSON(response);
                    updateUI(project);
                }

                @Override
                public void onFailure(VolleyError e) {
                    Log.e(TAG, "getProjectDetail: fail", e);
                }
            });
            isSaved = true;
        }
    }

    private void updateUI(ProjectModel project) {
        projectNameView.setText(project.getProjectName());
        for (int i = 0; i < customer.size(); i++) {
            if (customer.get(i).getCustomerId() == project.getCustomerId()) {
                spinnerCustomer.setSelection(i);
            }
        }
        for (int i = 0; i < salesperson.size(); i++) {
            if (salesperson.get(i).getId() == project.getSalespersonId())
                spinnerSalesperson.setSelection(i);
        }
    }

    private void attemptUpload() {
        if (isSaved) return;
    }

    private void InitializeInfo() {
        controller.queryProjectTable(new VolleyEventListener() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "queryProjectTable: success");
                customer = controller.buildCustomerListFromJSON(response);
                salesperson = controller.buildSalespersonListFromJSON(response);

                ArrayAdapter<CustomerModel> adapterCustomer = new ArrayAdapter<>(NewProject.this,android.R.layout.simple_spinner_item, customer);
                adapterCustomer.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerCustomer = findViewById(R.id.project_customer_spinner);
                spinnerCustomer.setAdapter(adapterCustomer);

                ArrayAdapter<SalespersonModel> adapterSalesperson = new ArrayAdapter<>(NewProject.this,android.R.layout.simple_spinner_item,salesperson);
                adapterSalesperson.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerSalesperson = findViewById(R.id.project_salesperson_spinner);
                spinnerSalesperson.setAdapter(adapterSalesperson);
            }

            @Override
            public void onFailure(VolleyError e) {
                Log.e(TAG, "queryProjectTable: fail", e);
                customer = new ArrayList<>();
                salesperson = new ArrayList<>();

                ArrayAdapter<CustomerModel> adapterCustomer = new ArrayAdapter<>(NewProject.this,android.R.layout.simple_spinner_item, customer);
                adapterCustomer.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerCustomer = findViewById(R.id.project_customer_spinner);
                spinnerCustomer.setAdapter(adapterCustomer);

                ArrayAdapter<SalespersonModel> adapterSalesperson = new ArrayAdapter<>(NewProject.this,android.R.layout.simple_spinner_item,salesperson);
                adapterSalesperson.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerSalesperson = findViewById(R.id.project_salesperson_spinner);
                spinnerSalesperson.setAdapter(adapterSalesperson);
            }
        });
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(), text, Toast.LENGTH_SHORT);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.Cus_drawer);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.nav_customer:
                Intent uploadIntent = new Intent(NewProject.this, InformationInput.class);
                startActivity(uploadIntent);
                break;

            case R.id.nav_project:
                break;

            case R.id.nav_setting:
                Intent settingIntent = new Intent(NewProject.this, UserSetting.class);
                startActivity(settingIntent);
                break;

            case R.id.nav_log_out:
                LoginController loginController = new LoginController();
                loginController.logout(this);
                Intent loginIntent = new Intent(NewProject.this, LoginActivity.class);
                startActivity(loginIntent);
                finish();
                break;
        }
        DrawerLayout drawer = findViewById(R.id.Cus_drawer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
