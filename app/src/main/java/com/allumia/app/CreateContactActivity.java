package com.allumia.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class CreateContactActivity extends AppCompatActivity {

    public static final String TAG = "CreateContactActivity";
    private CustomerController controller;

    private EditText mFirstName;
    private EditText mLastName;
    private EditText mEmail;
    private EditText mPhone;

    private int customerId;
    private int contactId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_contact);
        controller = new CustomerController(this);
        customerId = getIntent().getIntExtra("customerId", 0);
        contactId = 0;

        Toolbar toolbar = findViewById(R.id.create_contact_toolbar);
        setSupportActionBar(toolbar);

        mFirstName = findViewById(R.id.contact_first_name_input);
        mLastName = findViewById(R.id.contact_last_name_input);
        mEmail = findViewById(R.id.contact_email_input);
        mPhone = findViewById(R.id.contact_phone_input);

        Button saveButton = findViewById(R.id.contact_save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                attemptUpload();
            }
        });
        Button cancelButton = findViewById(R.id.contact_cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private boolean isEmailValid(String email) {
        return email.isEmpty() || email.contains("@");
    }

    private boolean isPhoneValid(String phone) {
        if (phone.isEmpty()) return true;
        for (char c : phone.toCharArray()) {
            if (!Character.isDigit(c))
                return false;
        }
        return true;
    }

    private void attemptUpload() {
        String firstName = mFirstName.getText().toString();
        String lastName = mLastName.getText().toString();
        String email = mEmail.getText().toString();
        String phone = mPhone.getText().toString();

        if (!isEmailValid(email)) {
            Toast toast = Toast.makeText(this, "Please input valid email address", Toast.LENGTH_SHORT);
            toast.show();
            mEmail.requestFocus();
        } else if (!isPhoneValid(phone)) {
            Toast toast = Toast.makeText(this, "Please use only digits for phone number", Toast.LENGTH_SHORT);
            toast.show();
            mPhone.requestFocus();
        } else if (firstName.isEmpty() && lastName.isEmpty() && email.isEmpty() && phone.isEmpty()) {
            Toast toast = Toast.makeText(this, "Please fill the contact info", Toast.LENGTH_SHORT);
            toast.show();
        } else {

            ContactModel contact = controller.createNewContact(0, firstName, lastName, email, phone);

            controller.uploadContact(this, contact, new VolleyEventListener() {
                @Override
                public void onSuccess(JSONObject response) {
                    Log.d(TAG, "create new contact: success");
                    try {
                        contactId = response.getJSONObject("result").getJSONObject("value").getInt("id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Toast toast = Toast.makeText(CreateContactActivity.this, "Contact saved", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.d(TAG, "new contact ID: " + contactId);

                    linkCustomerContact();

                    finish();
                }

                @Override
                public void onFailure(VolleyError e) {
                    Log.e(TAG, "create new contact: failed" , e);
                }
            });
        }
    }

    private void linkCustomerContact() {
        if (customerId == 0 || contactId == 0) {
            Log.e(TAG, "Unexpected call of linkCustomerContact");
        }
        controller.linkCustomerContact(this, customerId, contactId, new VolleyEventListener() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "Link customer and contact: success");

            }

            @Override
            public void onFailure(VolleyError e) {
                Log.e(TAG, "Link customer and contact: failed", e);
            }
        });
    }
}
