package com.allumia.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * LoginController controls login & log out activity
 */
public class LoginController {

    public static final String API_URL = "Account/Login";

    public void login(Context context, String email, String password, VolleyEventListener listener) {
        JSONObject credential = new JSONObject();
        try {
            credential.put("email", email);
            credential.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiManager.postRequest(context, API_URL, credential, listener);
    }

    public void logout(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

}
