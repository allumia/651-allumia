package com.allumia.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.allumia.app.model.ProjectModel;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.List;

public class ProjectFragment extends Fragment {

    public static final String TAG = "Project Fragment";
    public static final int ADD_PROJECT_REQUEST = 1;

    private RecyclerView recyclerView;
    private ProjectAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ProjectController controller;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_project, container, false);

        controller = new ProjectController(getActivity());
        updateProjectList();
        List<ProjectModel> projectList = controller.getProjectList();

        recyclerView = view.findViewById(R.id.recycler_view_project);
        layoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new ProjectAdapter(projectList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new ProjectAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int i) {
                ProjectModel project = controller.getProjectList().get(i);
                Intent showProjectIntent = new Intent(getActivity(), NewProject.class);
                showProjectIntent.putExtra(getString(R.string.lighting_project_id), project.getProjectId());
                startActivity(showProjectIntent);
            }
        });

        FloatingActionButton fab = view.findViewById(R.id.fab_project);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NewProject.class);
                startActivityForResult(intent, ADD_PROJECT_REQUEST);
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_PROJECT_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                updateProjectList();
            }
        }
    }

    private void updateProjectList() {
        controller.queryProjectList(new VolleyEventListener() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "updateProjectList: success");
                controller.saveProjectList(response);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(VolleyError e) {
                Log.e(TAG, "updateProjectList: fail", e);
            }
        });
    }
}
