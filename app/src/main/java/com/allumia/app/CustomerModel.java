package com.allumia.app;

/**
 * representing the fields of the table Customer (table ID: 2)
 */
public class CustomerModel {
    private int customerId;
    private String name;
    private int salesChannel;
    private String pitchDate;

    CustomerModel(int customerId, String name, int salesChannel, String pitchDate) {
        this.customerId = customerId;
        this.name = name;
        this.salesChannel = salesChannel;
        this.pitchDate = pitchDate;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalesChannel() {
        return salesChannel;
    }

    public void setSalesChannel(int salesChannel) {
        this.salesChannel = salesChannel;
    }

    public String getPitchDate() {
        return pitchDate;
    }

    public void setPitchDate(String pitchDate) {
        this.pitchDate = pitchDate;
    }

    public String getSalesChannelString() {
        if (salesChannel == 66) return "Direct Sales";
        else if (salesChannel == 67) return "Channel Sales";
        else if (salesChannel == 68) return "Door to Door Sales";
        else return "";
    }

    @Override
    public String toString() {
        return name;
    }
}
