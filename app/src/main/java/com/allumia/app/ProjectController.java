package com.allumia.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.allumia.app.model.ProjectModel;
import com.allumia.app.model.SalespersonModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ProjectController {

    public static final String TAG = "Project Controller";

    public static final String RECORD_API_URL = "Record";
    public static final String QUERY_PROJECT_URL = "Query/12/Result";

    public static final int PROJECT_TABLE_ID = 6;
    public static final int PROJECT_AREA_TABLE_ID = 15;
    public static final int PROJECT_SPACE_LIST_TABLE_ID = 11;

    private Context mContext;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Gson gson;
    private List<ProjectModel> projectList;

    public ProjectController(Context context) {
        mContext = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        editor = sharedPreferences.edit();
        gson = new Gson();
        String json = sharedPreferences.getString("projectList", null);
        Type type = new TypeToken<List<ProjectModel>>() {}.getType();
        projectList = gson.fromJson(json, type);
        if (projectList == null) {
            projectList = new ArrayList<>();
        }
    }

    public ProjectModel createNewProject(int projectId, String projectName,
                                         int customerId, String customerName,
                                         int salespersonId, String salespersonName) {
        return new ProjectModel(projectId, projectName, customerId, customerName, salespersonId, salespersonName);
    }

    public ProjectModel buildProjectFromJSON(JSONObject response) {
        int projectId = 0;
        int customerId = 0;
        int salespersonId = 0;
        String projectName = "";
        String customerName = "";
        String salespersonName = "";

        try {
            JSONObject jsonProject = response.getJSONObject("result").getJSONObject("value");
            projectId = jsonProject.getInt("id");
            JSONArray jsonDetail = jsonProject.getJSONArray("fields");
            for (int i = 0; i < jsonDetail.length(); i++) {
                JSONObject field = jsonDetail.getJSONObject(i);
                if (field.getInt("fieldDefinitionId") == 25)
                    projectName = field.getString("textValue");
                else if (field.getInt("fieldDefinitionId") == 28) {
                    customerId = field.getInt("integerValue");
                    JSONArray customerList = field.getJSONArray("lookups");
                    for (int j = 0; j < customerList.length(); j++) {
                        if (customerList.getJSONObject(j).getInt("lookup_ID") == customerId)
                            customerName = customerList.getJSONObject(j).getString("lookup_Name");
                    }
                }
                else if (field.getInt("fieldDefinitionId") == 155) {
                    salespersonId = field.getInt("integerValue");
                    JSONArray salesList = field.getJSONArray("lookups");
                    for (int j = 0; j < salesList.length(); j++) {
                        if (salesList.getJSONObject(j).getInt("lookup_ID") == salespersonId)
                            salespersonName = salesList.getJSONObject(j).getString("lookup_Name");
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new ProjectModel(projectId, projectName, customerId, customerName, salespersonId, salespersonName);
    }

    public List<CustomerModel> buildCustomerListFromJSON(JSONObject response) {
        List<CustomerModel> customerList = new ArrayList<>();
        try {
            JSONArray jsonDetail = response.getJSONArray("fields");
            for (int i = 0; i < jsonDetail.length(); i++) {
                JSONObject field = jsonDetail.getJSONObject(i);
                if (field.getInt("fieldDefinitionId") == 28) {
                    JSONArray customerJsonArray = field.getJSONArray("lookups");
                    for (int j = 0; j < customerJsonArray.length(); j++) {
                        int id = customerJsonArray.getJSONObject(j).getInt("lookup_ID");
                        String name = customerJsonArray.getJSONObject(j).getString("lookup_Name");
                        customerList.add(new CustomerModel(id, name, 0, ""));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return customerList;
    }

    public List<SalespersonModel> buildSalespersonListFromJSON(JSONObject response) {
        List<SalespersonModel> salespersonList = new ArrayList<>();
        try {
            JSONArray jsonDetail = response.getJSONArray("fields");
            for (int i = 0; i < jsonDetail.length(); i++) {
                JSONObject field = jsonDetail.getJSONObject(i);
                if (field.getInt("fieldDefinitionId") == 155) {
                    JSONArray customerJsonArray = field.getJSONArray("lookups");
                    for (int j = 0; j < customerJsonArray.length(); j++) {
                        int id = customerJsonArray.getJSONObject(j).getInt("lookup_ID");
                        String name = customerJsonArray.getJSONObject(j).getString("lookup_Name");
                        salespersonList.add(new SalespersonModel(id, name));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return salespersonList;
    }

    public List<ProjectModel> getProjectList() {
        return projectList;
    }

    public void queryProjectList(VolleyEventListener listener) {
        ApiManager.getRequest(mContext, QUERY_PROJECT_URL, null, listener);
    }

    public void saveProjectList(JSONObject response) {
        try {
            JSONArray projectJSONArray = response.getJSONArray("queryData");

            if (projectJSONArray != null) {
                projectList.clear();
                for (int i = 0; i < projectJSONArray.length(); i++) {
                    JSONObject item = projectJSONArray.getJSONObject(i);
                    if (item.get("project_Name") != JSONObject.NULL) {
                        int projectId = item.getInt("lighting_Project_ID");
                        String projectName = item.getString("project_Name");
                        int customerId = item.getInt("customer_ID");
                        String customerName = item.getString("customer_Name");
                        ProjectModel project = createNewProject(projectId, projectName, customerId, customerName, 0, "");
                        projectList.add(project);
                    }
                }

                editor.putString("projectList", gson.toJson(projectList));
                editor.apply();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void queryProjectTable(VolleyEventListener listener) {
        ApiManager.getRequest(mContext, RECORD_API_URL + "/Table/6", null, listener);
    }

    public void uploadNewProject(ProjectModel project, VolleyEventListener listener) {
        JSONObject requestObject = new JSONObject();
        JSONObject projectName = new JSONObject();
        JSONObject customer = new JSONObject();
        JSONObject salesperson = new JSONObject();
        JSONArray fields = new JSONArray();
        try {
            projectName.put("fieldDefinitionId", 25);
            projectName.put("textValue", project.getProjectName());
            fields.put(projectName);

            customer.put("fieldDefinitionId", 28);
            customer.put("integerValue", project.getCustomerId());
            fields.put(customer);


            salesperson.put("fieldDefinitionId", 155);
            salesperson.put("integerValue", project.getSalespersonId());
            fields.put(salesperson);

            requestObject.put("tableId", PROJECT_TABLE_ID);
            requestObject.put("fields", fields);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiManager.postRequest(mContext, RECORD_API_URL, requestObject, listener);
    }

    public void getProjectDetail(int projectId, VolleyEventListener listener) {
        ApiManager.getRequest(mContext, RECORD_API_URL + "/" + projectId, null, listener);
    }
}
