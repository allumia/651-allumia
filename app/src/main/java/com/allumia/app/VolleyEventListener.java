package com.allumia.app;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Callback interface for activities calling APIs
 */
public interface VolleyEventListener {

    void onSuccess(JSONObject response);

    void onFailure(VolleyError e);
}
