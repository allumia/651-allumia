package com.allumia.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.view.MenuItem;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class InformationInput extends AppCompatActivity implements AdapterView.OnItemSelectedListener,NavigationView.OnNavigationItemSelectedListener{

    private static final String TAG = "InformationInput";

    private Spinner mSalesSpinner;
    private EditText mCustomerName;
    private EditText mDate;
    private CustomerController controller;

    private boolean isSaved;
    private int customerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_input);
        controller = new CustomerController(this);

        NavigationView navigationView = findViewById(R.id.info_nav);
        navigationView.setNavigationItemSelectedListener(this);
        // Guide menu
        Toolbar toolbar = findViewById(R.id.activity_input_toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.Info_drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        //initiate member
        mCustomerName = findViewById(R.id.customer_name_input);
        mDate = findViewById(R.id.customer_pitch_date_input);
        mSalesSpinner = findViewById(R.id.customer_sales_channel_spinner);
        List<String> spinnerContent = new ArrayList<>();
        // TODO: Dynamic generate drop down list
        spinnerContent.add("");
        spinnerContent.add("Direct Sales");
        spinnerContent.add("Channel Sales");
        spinnerContent.add("Door to Door Sales");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, spinnerContent);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSalesSpinner.setAdapter(adapter);

        Button addContactButton = findViewById(R.id.add_contact_button);
        addContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // jump to add contact form
                if (isSaved) {
                    Intent intent = new Intent(InformationInput.this, CreateContactActivity.class);
                    intent.putExtra("customerId", customerId);
                    startActivity(intent);
                } else {
                    Toast toast = Toast.makeText(InformationInput.this, "Please save customer first", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
        Button uploadButton = findViewById(R.id.button);
        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attempt_Upload();
            }
        });

        Intent intent = getIntent();
        if (intent.getIntExtra(getString(R.string.customer_id), 0) != 0) {
            customerId = intent.getIntExtra(getString(R.string.customer_id), 0);
            mCustomerName.setText(intent.getStringExtra(getString(R.string.customer_name)));
            mDate.setText(intent.getStringExtra(getString(R.string.customer_first_pitch_date)));
            switch (intent.getStringExtra(getString(R.string.customer_sales_channel))) {
                case "Direct Sales":
                    mSalesSpinner.setSelection(1);
                    break;
                case "Channel Sales":
                    mSalesSpinner.setSelection(2);
                    break;
                case "Door to Door Sales":
                    mSalesSpinner.setSelection(3);
                    break;
                default:
                    mSalesSpinner.setSelection(0);
                    break;
            }
            isSaved = true;
        } else {
            isSaved = false;
            customerId = 0;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(), text, Toast.LENGTH_SHORT);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.Info_drawer);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent returnIntent = new Intent();
            if (isSaved) {
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            } else {
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.nav_customer:
                Intent uploadIntent = new Intent(InformationInput.this, InformationInput.class);
                startActivity(uploadIntent);
                break;

            case R.id.nav_project:
                break;

            case R.id.nav_setting:
                Intent settingIntent = new Intent(InformationInput.this, UserSetting.class);
                startActivity(settingIntent);
                break;

            case R.id.nav_log_out:
                LoginController loginController = new LoginController();
                loginController.logout(this);
                Intent loginIntent = new Intent(InformationInput.this, LoginActivity.class);
                startActivity(loginIntent);
                finish();
                break;
        }
        DrawerLayout drawer = findViewById(R.id.Info_drawer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private boolean checkRequiredFilled() {
        return !mCustomerName.getText().toString().isEmpty();
    }

    private boolean checkDateFormat(String date) {
        if (date.isEmpty()) return true;

        SimpleDateFormat sdf = new SimpleDateFormat(getString(R.string.date_input_format));
        sdf.setLenient(false);
        try {
            //if not valid, it will throw ParseException
            sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private void attempt_Upload() {
        // If current Customer is already created, reject attempt
        if (isSaved) return;

        final String customerName = mCustomerName.getText().toString();
        final String salesChannel = mSalesSpinner.getSelectedItem().toString();;
        final String pitchDate = mDate.getText().toString().trim();

        if (!checkRequiredFilled()) {
            AlertDialog.Builder InvalidAlert = new AlertDialog.Builder(InformationInput.this);
            InvalidAlert.setTitle("Customer name is required");
            InvalidAlert.setMessage("Please try again");
            InvalidAlert.setCancelable(true);
            InvalidAlert.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mCustomerName.requestFocus();
                }
            });
            InvalidAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            InvalidAlert.show();
        } else if (!checkDateFormat(pitchDate)) {
            AlertDialog.Builder InvalidAlert = new AlertDialog.Builder(InformationInput.this);
            InvalidAlert.setTitle("Date format is wrong");
            InvalidAlert.setMessage("Please try again");
            InvalidAlert.setCancelable(true);
            InvalidAlert.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mDate.requestFocus();
                }
            });
            InvalidAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            InvalidAlert.show();
        } else {
            AlertDialog.Builder confirmationDialog = new AlertDialog.Builder(InformationInput.this);
            confirmationDialog.setTitle("Create new customer: " + customerName);
            confirmationDialog.setCancelable(true);
            confirmationDialog.setPositiveButton("Create", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    final CustomerModel customer = controller.createNewCustomer(0, customerName, salesChannel, pitchDate);
                    controller.uploadNewCustomer(InformationInput.this, customer, new VolleyEventListener() {
                        @Override
                        public void onSuccess(JSONObject response) {
                            Log.d(TAG, "create new customer: success");
                            isSaved = true;
                            try {
                                customerId = response.getJSONObject("result").getJSONObject("value").getInt("id");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Toast toast = Toast.makeText(InformationInput.this, "Customer saved", Toast.LENGTH_SHORT);
                            toast.show();
                            Log.d(TAG, "customer ID: " + customerId);
                        }

                        @Override
                        public void onFailure(VolleyError e) {
                            Log.e(TAG, "create new customer: failed" , e);
                            Toast toast = Toast.makeText(InformationInput.this, "Save failed", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    });

//                    AlertDialog.Builder Upload_Succ = new AlertDialog.Builder(InformationInput.this);
//                    Upload_Succ.setTitle("Upload Success!");
//                    Upload_Succ.setCancelable(true);
//                    Upload_Succ.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            Intent intent = new Intent(InformationInput.this, Guidance.class);
//                            startActivity(intent);
//                        }
//                    });
//                    Upload_Succ.show();
                }
            });
            confirmationDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            confirmationDialog.show();
        }
    }

}
