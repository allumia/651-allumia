package com.allumia.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CustomerFragment extends Fragment {

    public static final String TAG = "CustomerFragment";
    public static final int ADD_CUSTOMER_REQUEST = 1;

    private RecyclerView recyclerView;
    private CustomerAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private CustomerController controller;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer, container, false);
        controller = new CustomerController(getActivity());
        updateCustomerList();

        List<CustomerModel> customerList = controller.getCustomerList();

        recyclerView = view.findViewById(R.id.recycler_view_customer);
        layoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new CustomerAdapter(customerList);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new CustomerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int i) {
                CustomerModel customer = controller.getCustomerList().get(i);
                Intent showCustomerIntent = new Intent(getActivity(), InformationInput.class);
                showCustomerIntent.putExtra(getString(R.string.customer_id), customer.getCustomerId());
                showCustomerIntent.putExtra(getString(R.string.customer_name), customer.getName());
                showCustomerIntent.putExtra(getString(R.string.customer_sales_channel), customer.getSalesChannelString());
                showCustomerIntent.putExtra(getString(R.string.customer_first_pitch_date), customer.getPitchDate());
                startActivity(showCustomerIntent);
            }
        });

        FloatingActionButton fab = view.findViewById(R.id.fab_customer);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), InformationInput.class);
                startActivityForResult(intent, ADD_CUSTOMER_REQUEST);
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_CUSTOMER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                updateCustomerList();
            }
        }
    }

    private void updateCustomerList() {
        controller.queryCustomerList(new VolleyEventListener() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "getCustomerList: success");
                controller.saveCustomerList(response);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(VolleyError e) {
                Log.e(TAG, "getCustomerList: fail", e);
            }
        });
    }
}
