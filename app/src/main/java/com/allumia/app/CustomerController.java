package com.allumia.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CustomerController {
    public static final String TAG = "Customer Controller";

    public static final String RECORD_API_URL = "Record";
    public static final String QUERY_CUSTOMER_URL = "Query/11/Result";

    public static final int CUSTOMER_TABLE_ID = 2;
    public static final int CUSTOMER_CONTACT_TABLE_ID = 3;
    public static final int CUSTOMER_CONTACT_LINK_TABLE_ID = 5;

    private Context mContext;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Gson gson;
    private List<CustomerModel> customerList;

    public CustomerController(Context context) {
        mContext = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        editor = sharedPreferences.edit();
        gson = new Gson();
        String json = sharedPreferences.getString("customerList", null);
        Type type = new TypeToken<List<CustomerModel>>() {}.getType();
        customerList = gson.fromJson(json, type);
        if (customerList == null) {
            customerList = new ArrayList<>();
        }
    }

    public CustomerModel createNewCustomer(int customerId, String name, String channel, String date) {
        return new CustomerModel(customerId, name, findSalesChannelByString(channel), date);
    }

    public ContactModel createNewContact(int contactId, String firstName, String lastName, String email, String phone) {
        return new ContactModel(contactId, firstName, lastName, email, phone);
    }

    public List<CustomerModel> getCustomerList() {
        return customerList;
    }

    public void queryCustomerList(VolleyEventListener listener) {
        ApiManager.getRequest(mContext, QUERY_CUSTOMER_URL, null, listener);
    }

    public void saveCustomerList(JSONObject response) {
        try {
            JSONArray customerJSONArray = response.getJSONArray("queryData");

            if (customerJSONArray != null) {
                customerList.clear();
                for (int i = 0; i < customerJSONArray.length(); i++) {
                    JSONObject item = customerJSONArray.getJSONObject(i);
                    if (item.get("customer_Name") != JSONObject.NULL) {
                        int id = item.getInt("customer_ID");
                        String customerName = item.getString("customer_Name");
                        String salesChannel = item.getString("sales_Channel");
                        String firstSalesPitch = item.getString("first_Sales_Pitch");
                        CustomerModel customer = createNewCustomer(id, customerName, salesChannel, firstSalesPitch);
                        customerList.add(customer);
                    }
                }

                editor.putString("customerList", gson.toJson(customerList));
                editor.apply();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void uploadNewCustomer(Context context, CustomerModel customer, VolleyEventListener listener) {
        JSONObject requestObject = new JSONObject();
        JSONObject name = new JSONObject();
        JSONObject salesChannel = new JSONObject();
        JSONObject pitchDate = new JSONObject();
        JSONArray fields = new JSONArray();
        try {
            name.put("fieldDefinitionId", 2);
            name.put("textValue", customer.getName());
            fields.put(name);

            if (customer.getSalesChannel() != 0) {
                salesChannel.put("fieldDefinitionId", 97);
                salesChannel.put("integerValue", customer.getSalesChannel());
                fields.put(salesChannel);
            }
            if (!customer.getPitchDate().isEmpty()) {
                pitchDate.put("fieldDefinitionId", 98);
                pitchDate.put("dateValue", customer.getPitchDate());
                fields.put(pitchDate);
            }

            requestObject.put("tableId", CUSTOMER_TABLE_ID);
            requestObject.put("fields", fields);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiManager.postRequest(context, RECORD_API_URL, requestObject, listener);
    }

    public void uploadContact(Context context, ContactModel contact, VolleyEventListener listener) {
        JSONObject requestObject = new JSONObject();
        JSONArray fields = new JSONArray();
        try {
            if (!contact.getFirstName().isEmpty()) {
                JSONObject firstName = new JSONObject();
                firstName.put("fieldDefinitionId", 4);
                firstName.put("textValue", contact.getFirstName());
                fields.put(firstName);
            }
            if (!contact.getLastName().isEmpty()) {
                JSONObject lastName = new JSONObject();
                lastName.put("fieldDefinitionId", 95);
                lastName.put("textValue", contact.getLastName());
                fields.put(lastName);
            }
            if (!contact.getEmail().isEmpty()) {
                JSONObject email = new JSONObject();
                email.put("fieldDefinitionId", 6);
                email.put("textValue", contact.getEmail());
                fields.put(email);
            }
            if (!contact.getPhone().isEmpty()) {
                JSONObject phone = new JSONObject();
                phone.put("fieldDefinitionId", 92);
                phone.put("textValue", contact.getPhone());
                fields.put(phone);
            }
            requestObject.put("tableId", CUSTOMER_CONTACT_TABLE_ID);
            requestObject.put("fields", fields);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiManager.postRequest(context, RECORD_API_URL, requestObject, listener);
    }

    public void linkCustomerContact(Context context, int customerId, int contactId, VolleyEventListener listener) {
        JSONObject requestObject = new JSONObject();
        JSONArray fields = new JSONArray();
        try {
            JSONObject customerIdObject = new JSONObject();
            customerIdObject.put("fieldDefinitionId", 22);
            customerIdObject.put("integerValue", customerId);
            JSONObject contactIdObject = new JSONObject();
            contactIdObject.put("fieldDefinitionId", 23);
            contactIdObject.put("integerValue", contactId);
            fields.put(customerIdObject);
            fields.put(contactIdObject);
            requestObject.put("tableId", CUSTOMER_CONTACT_LINK_TABLE_ID);
            requestObject.put("fields", fields);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiManager.postRequest(context, RECORD_API_URL, requestObject, listener);
    }

    private int findSalesChannelByString(String s) {
        switch (s) {
            case "Direct Sales":
                return 66;
            case "Channel Sales":
                return 67;
            case "Door to Door Sales":
                return 68;
            default:
                return 0;
        }
    }
}
