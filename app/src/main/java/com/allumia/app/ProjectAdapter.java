package com.allumia.app;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.allumia.app.model.ProjectModel;

import java.util.List;

public class ProjectAdapter extends RecyclerView.Adapter<ProjectAdapter.ProjectViewHolder> {
    private List<ProjectModel> mProjectList;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(int i);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public static class ProjectViewHolder extends RecyclerView.ViewHolder {

        public TextView projectNameView;
        public TextView customerNameView;

        public ProjectViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            projectNameView = itemView.findViewById(R.id.project_item_name);
            customerNameView = itemView.findViewById(R.id.project_item_customer_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public ProjectAdapter(List<ProjectModel> projectList) {
        mProjectList = projectList;
    }

    @NonNull
    @Override
    public ProjectViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.project_item, viewGroup, false);
        ProjectViewHolder projectViewHolder = new ProjectViewHolder(v, listener);
        return projectViewHolder;
    }

    @Override
    public void onBindViewHolder(ProjectViewHolder viewHolder, int i) {
        ProjectModel currentItem = mProjectList.get(i);

        viewHolder.projectNameView.setText(currentItem.getProjectName());
        viewHolder.customerNameView.setText(currentItem.getCustomerName());
    }

    @Override
    public int getItemCount() {
        return mProjectList.size();
    }

}
