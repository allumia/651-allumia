package com.allumia.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * API manager handles all API calls to Skylight.
 * Provides methods for each HTTP request: GET, POST, PUT, DELETE
 */
public class ApiManager {

    public static final String BASE_URL = "https://skylight-development-api.azurewebsites.net/api/";

    /**
     * Create a HTTP POST request to call the backend REST API
     *
     * @param context       Context of current activity
     * @param api           Path to API interface
     * @param jsonRequest   Parameters of the request
     * @param listener      Callback Listener to handle response
     */
    public static void postRequest(Context context, final String api, JSONObject jsonRequest, final VolleyEventListener listener) {

        HttpRequest(context, Request.Method.POST, api, jsonRequest, listener);
    }

    /**
     * Create a HTTP GET request to call the backend REST API
     *
     * @param context       Context of current activity
     * @param api           Path to API interface
     * @param jsonRequest   Parameters of the request
     * @param listener      Callback Listener to handle response
     */
    public static void getRequest(Context context, final String api, JSONObject jsonRequest, final VolleyEventListener listener) {

        HttpRequest(context, Request.Method.GET, api, jsonRequest, listener);
    }

    /**
     * Create a HTTP PUT request to call the backend REST API
     *
     * @param context       Context of current activity
     * @param api           Path to API interface
     * @param jsonRequest   Parameters of the request
     * @param listener      Callback Listener to handle response
     */
    /*public static void putRequest(Context context, final String api, JSONObject jsonRequest, final VolleyEventListener listener) {

        HttpRequest(context, Request.Method.PUT, api, jsonRequest, listener);
    }
    */

    /**
     * Create a HTTP DELETE request to call the backend REST API
     *
     * @param context       Context of current activity
     * @param api           Path to API interface
     * @param jsonRequest   Parameters of the request
     * @param listener      Callback Listener to handle response
     */
    public static void deleteRequest(Context context, final String api, JSONObject jsonRequest, final VolleyEventListener listener) {

        HttpRequest(context, Request.Method.DELETE, api, jsonRequest, listener);
    }

    /**
     * Create a HTTP request to call the backend API
     *
     * @param context       Context of current activity
     * @param api           Path to API interface
     * @param jsonRequest   Parameters of the request
     * @param listener      Callback Listener to handle response
     */
    public static void HttpRequest(final Context context, int method, final String api, JSONObject jsonRequest, final VolleyEventListener listener) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest request = new JsonObjectRequest(
                method,
                BASE_URL + api,
                jsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("API: " + api + " response", "success");
                        listener.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("API: " + api + " response", "failed");
                        listener.onFailure(error);
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                if (sharedPreferences.contains(context.getString(R.string.user_token)))
                    params.put("Authorization", "Bearer " + sharedPreferences.getString(context.getString(R.string.user_token), ""));
                params.put("X-Api-Key", "api-key");
                return params;
            }
        };

        requestQueue.add(request);
    }
}
