package com.allumia.app.model;

public class ProjectModel {

    private int projectId;
    private String projectName;
    private int customerId;
    private String customerName;
    private int salespersonId;
    private String salespersonName;

    public ProjectModel(int projectId, String projectName, int customerId, String customerName, int salespersonId, String salespersonName) {
        this.projectId = projectId;
        this.projectName = projectName;
        this.customerId = customerId;
        this.customerName = customerName;
        this.salespersonId = salespersonId;
        this.salespersonName = salespersonName;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getSalespersonId() {
        return salespersonId;
    }

    public void setSalespersonId(int salespersonId) {
        this.salespersonId = salespersonId;
    }

    public String getSalespersonName() {
        return salespersonName;
    }

    public void setSalespersonName(String salespersonName) {
        this.salespersonName = salespersonName;
    }

    @Override
    public String toString() {
        return projectName;
    }
}
