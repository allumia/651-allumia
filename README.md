﻿# Allumia App Project Overview


## **Support & User Manual**

This document summarizes the basic functionalities of our Android mobile app made for **Allumia** from a user perspective. The document is organized into
different sections which gives the user complete guidance to operate the app in an easy and seamless manner. In this document we discuss the major features
of our app and how an user can get started using it in a short amount of time. We also include links to relevant documents for users to delve further into
the working and architecture of the application. 



### **Introduction**

Allumia Mobile App provides users with the core functionalities of the Skylight Web API that can be easily accessed on mobile devices. This makes the
entire process of inputting data manually easy and convenient for daily users of the app. The app comes with functions for creating, editing, and downloading forms from the online database. The data which is input by the user will be synchronized with Skylight Web API through the upload function in real time. The app aims to improve the 
overall experience of energy data collection. We have strived to make the UI simple so that a user with limited technological exposure can use the application without any 
struggle. 


### **Installation & Configuration information of the software**

Currently, the app can be exported directly from **Android Studio** onto a local **Android** host. With one tap, the app will open up the **Login Page** through which the users can log in as different individuals with disparate accesses.

#### **Login Page**

After you open the app, the user **login-authentication** page will be loaded. There are two fields and one submit button.

1. **Email**: This is your personal **Skylight** email address for login. A Skylight email address is required for login.  
2. **Password**: A password for authentication.  
3. **Sign in**:  A button which the user can press to login and initiate authentication.


#### **Guidance Page**

After the login process is completed, the user will be prompted to the **Guidance Drawer**. Here, the userc an click the slide-down from the top left. Four options would be provided to the user.

1. **Upload files**: This options directs the user to the form selection page where the user can select the form he wants to upload.
2. **View history**: This option shows the recent activity history of the current authenticated user.
3. **Setting**: The settings option allows users to change settings of the current account. Necessary account settings can be changes by choosing this option.
4. **Share**: This options lets the current user share his/her information to others.


#### **Selection Page**

After selecting **Upload files**, the user will be directed to the form selection page. Here, the user can select the forms based on their particular needs. Mutliple form pages
are at available so that the user can select the one he/she needs to. The forms were developed according to the specifications provided by our client Allumia. The fields provided in the forms are the ones which our client thought to be most essential for the application.

#### **Customer Information**

This is the initial form all customers must fill out. The form fill-up is mandatory  and it has two required fields.
1. **Customer Name**: Full name of the customer must be entered
2. **Sales Channel**: The type of channel


### **Overall structure of code**


#### Front-end (Android app):

- **Guidance.java:**:               This is the guidance and support page which gives the users an introduction on how to use the application effectively.
- **InformationInput.java:**:       This is the information input page which allows users to input informations to fill up the forms and other necessary stuff.
- **LoginActivity.java:**:          This is the login page which allows users to log in and initiate authentication.
- **Output.java:**:                 This is the output page which shows the users the results they queried.
- **Transaction.java:**:            This is the Transactions page where actual transaction activities happen.


#### Key APIs into the back end

- **POST  /api/Account/Login**: For Login purpose



- **GET /api/Record/{recordId}**: Return information and fields related to a particular record id.
- **GET /api/Record/Table/{TableId}**: Return a new and empty record for a given table with a particular table id.
- **PUT /api/Record/{recordId}**: This API call is useful for saving a particular record id.
    An example object that would save an existing Customer
```json
{
  "id": 555,
  "tableId": 2,
  "fields": [
    {
      "id": 1266,
      "recordId": 555,
      "fieldDefinitionId": 2,
      "textValue": "Edited API Created Customer"
    }
  ]
}
```

- **POST /api/Record/{recordId}**: To create a new record with different attributes and a record id.  
    An example object that would create a new Customer with the name “APITESTCUSTOMER”  

```json
{
  "id": 0,
  "tableId": 2,
  "fields": [
    {
      "id": 0,
      "recordId": 0,
      "fieldDefinitionId": 2,
      "textValue": "APITESTCUSTOMER" 
    }
  ]
}
```




### **Links to additional files**

If a user is interested in further understanding the requirements, architecture and the way this app was tested, they can visit the following links

- **Requirements:** https://drive.google.com/drive/folders/19MUm3-kPC53Bnn5OQALmpYAjC4b2U3vl


- **Architecture:**
https://drive.google.com/drive/folders/19PB4Z6YI8VqYEiWOeB0uM6XsHVkaqAEx


- **Test Plan:**
https://drive.google.com/file/d/11eJojpRJlV26OEgoXcr1pLq23xaWhp69/view?usp=sharing

